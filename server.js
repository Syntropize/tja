#!/bin/env node

(function(){
  'use strict';

  var server = require('./src/server');
  var socket = require('./src/socket');
  socket(server);

})(); 