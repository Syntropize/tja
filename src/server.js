(function(){
  'use strict';

  var fs = require('graceful-fs');
  var path = require('path');
  var url = require('url');
  var os = require('os');
  var mime = require('mime-types');
  var destroy = require('destroy');
  var http = require('http');
  var onFinished = require('on-finished');

  var constants = require('./constants');

  var HOME = constants.HOME_DIR;
  var IP = constants.IP;
  var PORT = constants.PORT;

  var homepage = {};

  var homepath = path.join(HOME,'index.html');

  var homepage = function homepage(status, errorcode) {
    var start = 
      '<header>' +
      '<h1>Syntropize</h1>' +
      '<h2>Re-imagine the Desktop!</h2>' +
      '</header>';

    switch (status) {
      case 'invalid': return(
        '<link rel="stylesheet" href="css/home.css">' + 
        '<div class="main">' +
        start +
        '<article class="overview">' + 
        '<p>Error: 404 - Invalid Location</p>' +
        '<p>Error Code: <code>' + errorcode + '</code></p>' +
        '</article></div>'
      );
      case 'missing': 
      default: return(  
        '<link rel="stylesheet" href="css/home.css">' +
        '<div class="main">' +
        start +
        '<article class="overview">' + 
        '<p>Error: 500 - Server Failure!</p>' +
        '<p>Error Code: <code>' + errorcode + '</code></p>'+
        '<br><br>' + 
        '<p>To view the contents at this location please download Syntropize from</p>' +
        '<p><a href="http://www.syntropize.ml">' +
        '<code>http://www.syntropize.ml</code></a></p>' +
        '</article></div>'  
      );  
    }
  };
          
  var server = http.createServer(function(req,res) {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','POST, GET, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Credentials',true);
    res.setHeader('Access-Control-Max-Age','86400'); // 24 hours
    res.setHeader('Access-Control-Allow-Headers','X-Requested-With, Access-Control-Allow-Origin, X-HTTP-Method-Override, Content-Type, Authorization, Accept');
  });
   
  server.on('request',function(request,response) {
    var result = {};
    var isFile = false;
    var urlObj = url.parse(request.url);
    var pathname = decodeURI(urlObj.pathname);

    /* Remove leading slash for windows in localhost scenario*/ 
    if (os.type() === 'Windows_NT' && HOME === '') {pathname = pathname.slice(1);}
    
    try {
      result.data = 'invalid';
      result.mime = 'text/html';
      result.stat = fs.lstatSync(HOME + pathname);
      if (result.stat.isFile()) {
        switch(path.extname(pathname)) {
          case '.card':
          case '.meta': 
            result.mime = 'application/json';
            break;
          default:
            result.mime = mime.lookup(pathname) || 'application/octet-stream';             
        }
        pathname = HOME + pathname;
        result.statusCode = 200;
      }
      else {
        result.data = 'missing';
        result.mime = 'text/html';
        result.stat = fs.lstatSync(homepath);
        pathname = homepath;
        result.statusCode = 200;
      }
    }
    catch(error) {
      result.data = homepage(result.data,error.code);
      result.statusCode = (result.data === 'missing') ? 500:404;
    }

    response.statusCode = result.statusCode;
    response.setHeader('Content-Type',result.mime);
    
    if (result.statusCode >= 400) {
      response.write(result.data);
      response.end();
    }
    else {
      response.setHeader('Content-Length',result.stat.size.toString());
      
      var errorHandler = function errorHandler(error){
        response.statusCode = 500;
        console.error('Serve Error:\n', error);
        response(end);
      };
      
      var fReadStream = fs.createReadStream(pathname, {bufferSize: 64 * 1024});
      
      onFinished(response, function (error) {
        destroy(fReadStream);
      });

      fReadStream
      .on('error', errorHandler)
      .pipe(response)
      .on('error', errorHandler);
    }
  });

  server.listen(PORT, IP, function() {
    console.log('Listening on ',IP,':',PORT);
  });

  exports = module.exports = server;

})();