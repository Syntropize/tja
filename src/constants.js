(function(){
	'use strict';

	var path = require('path');


	var PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
	var IP  = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
	var DATA_DIR = process.env.STORAGE ? process.env.STORAGE:path.join(process.cwd(),'test');
	var HOME_DIR = path.join(DATA_DIR,'data');

	var constants = {
		IP:IP,
		PORT:PORT,
		DATA_DIR:DATA_DIR,
		HOME_DIR:HOME_DIR
	};

	module.exports = exports = constants;

})();