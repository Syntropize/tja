(function(){ 
  'use strict'; 

  var fs = require('fs');
  var libPath = require('path');
  var os = require('os');
  var sio = require('socket.io');

  var watcher = require('./watcher');
  var constants = require('./constants');

  var HOME = constants.HOME_DIR;

  var locationConvertor = function(namespace) {
    return libPath.join(HOME,decodeURI(namespace.slice(1)));
  }; 

  var socketio =  function(server) {

    var io = sio(server);

    io.useNamespaceValidator(function(nsp,next) {
      //URL encode special characters - decode for obtaining filePaths
      var location = locationConvertor(nsp); 

      //For Local Windows Server
      if (os.type() === 'Windows_NT' && HOME === '') {location = location.slice(1);}
      try{
        var result = fs.lstatSync(location);
        if (result.isDirectory()) {next();}
        else {next('file');} 
      }
      catch(error) {
        //Do not pass full server path which may contain sensitive information
        error.path = libPath.relative(HOME,error.path);
        error.message = error.message.split(', ',1)[0] + ', ' + error.syscall + ' \'' + error.path + '\''; 
        delete error.stack;
        next(error);
      }
    })
    .on('connect', function (socket) {
      watcher(socket,locationConvertor(socket.nsp.name));
    })
    .on('namespaceConnect', function (socket) {
      watcher(socket,locationConvertor(socket.nsp.name));
    })
    ;
    return io;
  };
   
  module.exports = exports = socketio;

})();  